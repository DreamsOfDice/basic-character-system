using System;

namespace CharacterSystem.Templates {
    [Serializable]
    public class CharacterStatNameContainer {
        public int index;
        public CharacterStatTypeSO template;
        public string inspectorStatName; //only used for inspector visibility

        public CharacterStatNameContainer GetCopy()
        {
            CharacterStatNameContainer containerCopy = new CharacterStatNameContainer();
            containerCopy.index = index;
            containerCopy.template = template;
            containerCopy.inspectorStatName = inspectorStatName;
            return containerCopy;
        }

    }
}