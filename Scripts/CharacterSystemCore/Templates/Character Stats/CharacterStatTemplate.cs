using System;
using CharacterSystem.Skills;
using UnityEngine;

namespace CharacterSystem.Templates {
    [Serializable]
    public class CharacterStatTemplate : ICharacterStat {
        [SerializeField] protected CharacterStatNameContainer statType; //custom drawer for this
        [SerializeField] protected int statValue;

        protected Action<ICharacterStat> statChangeEvent;

        public object StatType => statType.template.statTypeName;

        public Sprite StatIcon => statType.template.statTypeSprite;

        public virtual int StatValue {
            get => statValue;
            set => ChangeValue(value);
        }

        public ICharacterStat GetStatCopy() {
            var copy = new CharacterStatTemplate();
            copy.statType = statType.GetCopy();
            copy.statValue = statValue;
            return copy;
        }

        public void RegisterToStatChange(Action<ICharacterStat> method) {
            statChangeEvent -= method;
            statChangeEvent += method;
        }

        protected virtual void ChangeValue(int amount) {
            statValue += amount;
            if (statChangeEvent != null) statChangeEvent.Invoke(this);
        }
    }
}