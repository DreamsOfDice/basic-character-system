using UnityEngine;

namespace CharacterSystem.Templates {
    [CreateAssetMenu(fileName = "NewStatType", menuName = "Character System/CharacterStatType")]
    public class CharacterStatTypeSO : ScriptableObject {
        public string statTypeName;
        public Sprite statTypeSprite;
    }
}