using System;
using CharacterSystem.Leveling;
using UnityEngine;

namespace CharacterSystem.Templates {
    [Serializable]
    public class CharacerLevelTemplate : ILevelData {
        [SerializeField] private CharacterExperienceTemplate experience;
        [SerializeField] private int currentLevel;
        [SerializeField] private int maxLevel;

        public IExperience Experience => experience;

        public object LevelGrowth => throw new NotImplementedException();

        public int CurrentLevel => currentLevel;

        public int MaxLevel => maxLevel;
    }
}