using System;
using CharacterSystem.Leveling;
using UnityEngine;

namespace CharacterSystem.Templates {
    [Serializable]
    public class CharacterExperienceTemplate : IExperience {
        [SerializeField] protected int currentExperience;
        [SerializeField] protected int totalExperience;
        [SerializeField] protected int targetExperience;
        protected Action<IExperience> experienceChangeEvent;

        public int CurrentExperience => currentExperience;

        public int TotalExperience => totalExperience;

        public int TargetExperience => targetExperience;

        public void GainExperience(int amount) {
            currentExperience += amount;
            totalExperience += amount;
            if (currentExperience > targetExperience) {
            }
        }

        public void LoseExperience(int amount) {
            currentExperience -= amount;
            totalExperience -= amount;
            if (currentExperience < 0) {
                totalExperience += currentExperience * -1;
                currentExperience = 0;
            }
        }

        public void RegisterToExperienceChange(Action<IExperience> method) {
            experienceChangeEvent -= method;
            experienceChangeEvent += method;
        }
    }
}