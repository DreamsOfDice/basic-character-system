﻿using System;
using System.Collections.Generic;
using CharacterSystem.CharacterBasics;
using CharacterSystem.Leveling;
using CharacterSystem.Skills;
using UnityEngine;

namespace CharacterSystem.Templates {
    public class CharacterTemplate : ICharacterData {
        protected List<ICharacterSkillData> characterSkills = new List<ICharacterSkillData>();
        protected ILevelData currentLevelData;
        protected List<ICharacterStat> currentStats = new List<ICharacterStat>();
        protected CharacterTemplateSO template;

        public CharacterTemplate(CharacterTemplateSO newTemplate) {
            template = newTemplate ?? throw new Exception("You can´t create a Character without a template");
            currentLevelData = template.levelGrowth;
            foreach (ICharacterStat stat in template.initialStats) currentStats.Add(stat.GetStatCopy());
        }

        #region ICharacterData

        public string CharacterName => template.characterName;

        public List<ICharacterSkillData> Skills => characterSkills;

        public ILevelData LevelInformation => currentLevelData;

        public Sprite CharacterIcon => template.characterPortriat;

        public List<ICharacterStat> OriginalStats => template.initialStats.ConvertAll(x => (ICharacterStat)x);

        public List<ICharacterStat> CurrentStats => currentStats;

        #endregion
    }
}