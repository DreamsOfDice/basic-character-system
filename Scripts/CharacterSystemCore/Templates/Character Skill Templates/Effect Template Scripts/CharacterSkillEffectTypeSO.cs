using UnityEngine;

namespace CharacterSystem.Skills {
    [CreateAssetMenu(fileName = "CharacterSkillEffectTypeSO",
        menuName = "Character System/Character Skills/CharacterSkillEffectTypeSO")]
    public class CharacterSkillEffectTypeSO : ScriptableObject {
        public string effectTypeName;
        public Sprite effectTypeIcon;
    }
}