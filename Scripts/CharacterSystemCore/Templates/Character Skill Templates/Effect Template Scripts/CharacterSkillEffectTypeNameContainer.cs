using System;

namespace CharacterSystem.Skills {
    [Serializable]
    public class CharacterSkillEffectTypeNameContainer {
        public int index;
        public CharacterSkillEffectTypeSO template;
        public string inspectorEffectTypeName; //only used for inspector visibility
  
    }
}