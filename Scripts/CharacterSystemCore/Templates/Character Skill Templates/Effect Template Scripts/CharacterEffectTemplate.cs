using System;
using UnityEngine;

namespace CharacterSystem.Skills {
    [Serializable]
    public class CharacterEffectTemplate : ICharacterSkillEffect {
        [SerializeField] private CharacterEffectNameContainer effectBaseInfoContainer = new CharacterEffectNameContainer();
        [SerializeField] private int effectValue;
        [SerializeField] private int durationValue;

        public CharacterEffectTemplate(CharacterSkillEffectBaseInfoSO template) {
            effectBaseInfoContainer.SkillEffectBaseInfoBaseData = template;
        }

        public CharacterEffectTemplate(CharacterEffectTemplate original) {
            effectBaseInfoContainer.SkillEffectBaseInfoBaseData =
                original.effectBaseInfoContainer.SkillEffectBaseInfoBaseData;
            effectValue = original.effectValue;
            durationValue = original.durationValue;
        }
        public string EffectName => effectBaseInfoContainer.SkillEffectBaseInfoBaseData.EffectName;

        public string EffectDescription => effectBaseInfoContainer.SkillEffectBaseInfoBaseData.EffectDescription;
        
        public object TypeOfEffect => effectBaseInfoContainer.SkillEffectBaseInfoBaseData.EffectType;

        public int EffectValue
        {
            get => effectValue;
            set => effectValue = value;
        }

        public int DurationValue
        {
            get => durationValue;
            set => durationValue = value;
        }
    }
}