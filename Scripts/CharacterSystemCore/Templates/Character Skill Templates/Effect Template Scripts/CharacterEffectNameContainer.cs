using System;
using CharacterSystem.Skills;
using Unity.Collections;
using UnityEngine;

[Serializable]
public class CharacterEffectNameContainer {
    public CharacterSkillEffectBaseInfoSO skillEffectBaseInfoBaseData;
    public string effectInspectorName;
    public int index;
    public string effectName;
    [TextArea]public string effectDescription;
    
    public CharacterSkillEffectBaseInfoSO SkillEffectBaseInfoBaseData {
        get => skillEffectBaseInfoBaseData;
        set => SetupInspectorWithSO(value);
    }

    private void SetupInspectorWithSO(CharacterSkillEffectBaseInfoSO newValue) {
        skillEffectBaseInfoBaseData = newValue;
        effectDescription = skillEffectBaseInfoBaseData.EffectDescription;
        effectName = skillEffectBaseInfoBaseData.EffectName;
        effectInspectorName = skillEffectBaseInfoBaseData.EffectName;
    }
}