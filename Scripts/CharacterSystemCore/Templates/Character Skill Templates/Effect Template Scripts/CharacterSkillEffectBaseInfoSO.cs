using System.Collections.Generic;
using UnityEngine;

namespace CharacterSystem.Skills {
    [CreateAssetMenu(fileName = "CharacterEffectSO", menuName = "Character System/Character Skills/CharacterEffectSO")]
    public class CharacterSkillEffectBaseInfoSO : ScriptableObject {
        [SerializeField] private string effectName;
        [SerializeField] [TextArea] private string effectDescription;
        [SerializeField] private CharacterSkillEffectTypeNameContainer effectType;

        public string EffectName {
            get => effectName;
            set => effectName = value;
        }

        public string EffectDescription {
            get => effectDescription;
            set => effectDescription = value;
        }

        public CharacterSkillEffectTypeNameContainer EffectType {
            get => effectType;
            set => effectType = value;
        }
    }
}