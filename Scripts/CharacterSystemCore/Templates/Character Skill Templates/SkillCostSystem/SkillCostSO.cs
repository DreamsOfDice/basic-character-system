using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterSystem.Skills
{
    [CreateAssetMenu(fileName = "CharacterSkillCostSO", menuName = "Character System/Character Skills/CharacterSkillCostSO")]
    public class SkillCostSO : ScriptableObject
    {
        [SerializeField] private string skillCostName;
        [SerializeField] [TextArea] private string skillCostDescription;

        public string SkillCostName
        {
            get => skillCostName;
            set => skillCostName = value;
        }

        public string SkillCostDescription
        {
            get => skillCostDescription;
            set => skillCostDescription = value;
        }
    }
}