using System;
using System.Collections;
using System.Collections.Generic;
using CharacterSystem.Skills;
using UnityEngine;

namespace CharacterSystem.Skills.Costs
{

    [Serializable]
    public class SkillCostTemplate : ISkillCost
    {
        [SerializeField] protected SkillCostNameContainer skillCostNameContainer = new SkillCostNameContainer();
        [SerializeField] private int costAmount;


        public string CostID => skillCostNameContainer.skillCostName;
        public int CostAmount { get; }

        public SkillCostNameContainer CostNameContainer => skillCostNameContainer;

        public SkillCostTemplate(SkillCostSO templateCostSO)
        {
            skillCostNameContainer.SkillCostBaseData = templateCostSO;
        }
        public SkillCostTemplate(SkillCostTemplate templateCostSO)
        {
            skillCostNameContainer.SkillCostBaseData = templateCostSO.skillCostNameContainer.SkillCostBaseData;
            costAmount = templateCostSO.costAmount;
        }


    }
}