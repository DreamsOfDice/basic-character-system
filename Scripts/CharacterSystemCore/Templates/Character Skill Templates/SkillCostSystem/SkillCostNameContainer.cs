using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterSystem.Skills
{
    [Serializable]
    public class SkillCostNameContainer
    {
        public SkillCostSO skillCostBaseData;
        public string skillCostName;
        public int index;
        [TextArea]public string skillCostDescription;
        public string skillCostInspectorName;
        public SkillCostSO SkillCostBaseData
        {
            get => skillCostBaseData;
            set => SetupInspectorWithSO(value);
        }

        private void SetupInspectorWithSO(SkillCostSO newValue) {
            skillCostBaseData = newValue;
            if (skillCostBaseData != null)
            {
                skillCostDescription = skillCostBaseData.SkillCostDescription;
                skillCostName = skillCostBaseData.SkillCostName;
                skillCostInspectorName = skillCostBaseData.SkillCostName;
            }
        }

    }
}