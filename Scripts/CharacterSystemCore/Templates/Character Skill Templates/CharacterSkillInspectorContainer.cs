using System;

namespace CharacterSystem.Skills {
    [Serializable]
    public class CharacterSkillInspectorContainer {
        public string inspectorSkillName;
        public int index;
        public CharacterSkillSO template;//change this from SO
        public bool overrideSkillData = false;
        public CharacterSkillSO Template {
            get => template;
            set => SetupSkillWithTemplate(value);
        }

        public void SetupSkillWithTemplate(CharacterSkillSO skillTemplate)
        {
            template = skillTemplate;
            inspectorSkillName = template.SkillName;
        }
    }
}