using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills.Costs;
using CharacterSystem.Templates;
using UnityEngine;

namespace CharacterSystem.Skills {
    [CreateAssetMenu(fileName = "CharacterSkillSO", menuName = "Character System/Character Skills/CharacterSkillSO")]
    public class CharacterSkillSO : ScriptableObject, ICharacterSkillSOData //maybe this is overkill, composition might be the way to go... found SOs children searchable with assetdata search
    {
        [SerializeField] protected string skillName;
        [SerializeField] protected string skillDescription;
        [SerializeField] protected List<CharacterEffectNameContainer> baseEffects = new List<CharacterEffectNameContainer>();//need to add a new container for this that holds it and the read only name plus read only desc
        [SerializeField] protected List<CharacterSkillTypeTemplate> skillTypes = new List<CharacterSkillTypeTemplate>();
        [SerializeField] protected List<SkillCostNameContainer> skillCosts = new List<SkillCostNameContainer>();

        public string SkillName => skillName;
        public string SkillDescription => skillDescription;
        public List<ISkillCost> SkillCosts =>GetSkillCostFromContainer();
        public virtual List<ICharacterSkillEffect> SkillEffects => GetEffectSOsFromContainer();

        public List<CharacterSkillTypeTemplate> SkillTypes => skillTypes.ToList();

        protected virtual List<ICharacterSkillEffect> GetEffectSOsFromContainer() {
            List<ICharacterSkillEffect> effectsToReturn = new List<ICharacterSkillEffect>();
            if (baseEffects != null && baseEffects.Count > 0)
            {
                foreach (var effect in baseEffects)
                {
                    effectsToReturn.Add(new CharacterEffectTemplate(effect.SkillEffectBaseInfoBaseData));
                }
            }

            return effectsToReturn;
        }

        protected virtual List<ISkillCost> GetSkillCostFromContainer()
        {
            List<ISkillCost> skillCostToReturn = new List<ISkillCost>();
            if (skillCosts != null)
            {
                foreach (var skillCost in skillCosts)
                {
                    skillCostToReturn.Add(new SkillCostTemplate(skillCost.SkillCostBaseData));
                }
            }

            return skillCostToReturn;
        }
    }
}