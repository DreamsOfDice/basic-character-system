using System;
using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills.Costs;
using CharacterSystem.Templates;
using UnityEngine;
using UnityEngine.Serialization;

namespace CharacterSystem.Skills
{
    [Serializable]
    public class CharacterSkillTemplate : ICharacterSkillData
    {
        [FormerlySerializedAs("template")] [SerializeField]
        protected CharacterSkillInspectorContainer
            baseInfoContainer = new CharacterSkillInspectorContainer(); //might be wrong

        [SerializeField]
        protected List<CharacterSkillTypeTemplate> skillTypes = new List<CharacterSkillTypeTemplate>();

        [SerializeField] protected List<CharacterEffectTemplate> effects = new List<CharacterEffectTemplate>();
        [SerializeField] protected List<SkillCostTemplate> skillCosts = new List<SkillCostTemplate>();

        protected virtual CharacterSkillSO Template
        {
            get => (CharacterSkillSO)baseInfoContainer.Template;
            set => SetupFromTemplate(value);
        }

        public CharacterSkillTemplate(CharacterSkillSO template)//takes SO
        {
            Template = template;
        }

#if UNITY_EDITOR

        public virtual void SetupInspectorData()
        {
            if (baseInfoContainer.overrideSkillData)
            {
                return;
            }

            if (baseInfoContainer.template == null)
            {
                effects = new List<CharacterEffectTemplate>();
                skillTypes = new List<CharacterSkillTypeTemplate>();
                skillCosts = new List<SkillCostTemplate>();
                return;
            }
            baseInfoContainer.inspectorSkillName = baseInfoContainer.template.SkillName;
            SetupBaseEffects();
            SetupSkillTypes();
            SetupBaseCosts();
        }

        public bool CheckEffectsInitiated()
        {
            if (effects.Count != baseInfoContainer.template.SkillEffects.Count)
            {
                return false;
            }

            return true;
        }

        public bool IsEffectsOverriden()
        {
            return baseInfoContainer.overrideSkillData;
        }

        protected virtual void SetupBaseEffects()//You cant have a veriant of a skill with less base effects than the original, so you will check the list, and if it does not contain all template effects, it will be reset
        {
            //get all effects in the SO
            if (baseInfoContainer.Template == null)
            {
                return;
            }

            if (effects.Count != Template.SkillEffects.Count)
            {
                InitializeAllEffectsFromScratch();
                return;
            }


            for (int i = 0; i < Template.SkillEffects.Count; i++)
            {
                bool foundMatch = false;
                foreach (var effect in effects)
                {
                    if (effect.EffectName == Template.SkillEffects[i].EffectName)
                    {
                        foundMatch = true;
                        break;
                    }
                }

                if (foundMatch == false) 
                {
                    InitializeAllEffectsFromScratch();
                    return;
                }

            }
        }

        private void InitializeAllEffectsFromScratch()
        {
            effects = new List<CharacterEffectTemplate>();
            foreach (var effectTemplate in baseInfoContainer.template.SkillEffects)
            {
                effects.Add(new CharacterEffectTemplate((CharacterEffectTemplate)effectTemplate));
            }
        }

        protected virtual void SetupBaseCosts()
        {
            //get all effects in the SO
            if (baseInfoContainer.Template == null)
            {
                return;
            }

            skillCosts = new List<SkillCostTemplate>();
            foreach (var cost in baseInfoContainer.Template.SkillCosts)
            {
                skillCosts.Add(new SkillCostTemplate((SkillCostTemplate)cost));
            }
        }

        protected void SetupSkillTypes() //THIS ONE IS DIFFERENT, ALL SHOULD COPY
        {
            if (baseInfoContainer.Template == null)
            {
                return;
            }

            skillTypes = new List<CharacterSkillTypeTemplate>();
            foreach (var typeContainer in baseInfoContainer.template.SkillTypes)
            {
                skillTypes.Add(typeContainer);
            }
        }

#endif

        public virtual string SkillName => Template.SkillName;

        public virtual string SkillDescription => baseInfoContainer.Template.SkillDescription;

        public Sprite SkillIcon => throw new NotImplementedException();
        public List<ICharacterSkillType> SkillTypes => skillTypes.Cast<ICharacterSkillType>().ToList();

        public List<ICharacterSkillEffect> Effects => effects.Cast<ICharacterSkillEffect>().ToList();

        public CharacterSkillTemplate GetCopy()
        {
            CharacterSkillTemplate skillCopy = new CharacterSkillTemplate(Template);
            return skillCopy;
        }
        public List<SkillCostTemplate> SkillCosts => skillCosts;

        protected virtual void SetupFromTemplate(ICharacterSkillSOData skillTemplate)
        {
            /*if (baseInfoContainer == null) {
                return;
            }*/
            baseInfoContainer.SetupSkillWithTemplate((CharacterSkillSO)skillTemplate);
            effects = Template.SkillEffects.Cast<CharacterEffectTemplate>().ToList();
            skillCosts = Template.SkillCosts.Cast<SkillCostTemplate>().ToList();
        }
    }
}