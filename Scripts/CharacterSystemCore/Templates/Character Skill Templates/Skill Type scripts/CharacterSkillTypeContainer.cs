using System;
using System.Collections;
using System.Collections.Generic;
using CharacterSystem.Skills;
using UnityEngine;
using UnityEngine.Serialization;

namespace CharacterSystem.Templates {

    [Serializable]
    public class CharacterSkillTypeContainer  {
        public int index;
        public CharacterSkillTypeSO scriptableObjTemplate;
        public string inspectorSkillTypeName; //only used for inspector visibility

        public CharacterSkillTypeContainer CloneContainer()
        {
            CharacterSkillTypeContainer cloneSkillType = new CharacterSkillTypeContainer();
            cloneSkillType.scriptableObjTemplate = scriptableObjTemplate;
            cloneSkillType.index = index;
            cloneSkillType.inspectorSkillTypeName = inspectorSkillTypeName;
            return cloneSkillType;
        }
        
    }
}