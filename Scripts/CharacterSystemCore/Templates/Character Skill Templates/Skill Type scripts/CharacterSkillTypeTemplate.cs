using System;
using System.Collections;
using System.Collections.Generic;
using CharacterSystem.Skills;
using UnityEngine;

namespace CharacterSystem.Templates {
    
    [Serializable]
    public class CharacterSkillTypeTemplate : ICharacterSkillType {
        [SerializeField] private CharacterSkillTypeContainer skillTypeName;

        public string SkillTypeName => skillTypeName.scriptableObjTemplate.skillTypeName;
        public string SkillTypeDescription  => skillTypeName.scriptableObjTemplate.skillTypeDescription;
        public Sprite SkillTypeIcon  => skillTypeName.scriptableObjTemplate.statTypeIcon;
        
    }
}