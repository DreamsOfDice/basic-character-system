using System.Collections;
using System.Collections.Generic;
using CharacterSystem.Templates;
using UnityEngine;

namespace CharacterSystem.Skills {
    [CreateAssetMenu(fileName = "CharacterSkillTypeSO", menuName = "Character System/Character Skills/CharacterSkillTypeSO")]
    public class CharacterSkillTypeSO : ScriptableObject {
        public string skillTypeName;
        public string skillTypeDescription;
        public Sprite statTypeIcon;
    }
}