using System;
using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills;
using UnityEngine;

namespace CharacterSystem.Templates {
    [CreateAssetMenu(fileName = "New Character", menuName = "Character System/CharacterTemplate")]
    public class CharacterTemplateSO : ScriptableObject {
        public string characterName;
        public Sprite characterPortriat;
        public CharacerLevelTemplate levelGrowth;
        public List<CharacterStatTemplate> initialStats = new List<CharacterStatTemplate>();
        public List<CharacterSkillTemplate> initialSkills = new List<CharacterSkillTemplate>();
        
        private List<CharacterSkillTemplate> skillsSavedList = new List<CharacterSkillTemplate>();
        protected bool changing = false;
        protected virtual void OnValidate() {
            if (changing) {
                Debug.Log("Currently changing inspector value");
                return;
            }

            if (CheckSkillListMatch()) {
                Debug.Log("No difference");
                return;
            }

            changing = true;
            Debug.Log("You just did somethin");
            //make it so when you add a skill template, on add, effects are added. 
            foreach (var CharacterSkillTemplate in initialSkills) {
                if (CharacterSkillTemplate.IsEffectsOverriden()) {
                    continue;
                }
                CharacterSkillTemplate.SetupInspectorData();
            }
            //copy to list
            skillsSavedList = initialSkills.ToList();
            changing = false;
        }

        bool CheckSkillListMatch() {
            if (initialSkills.Count != skillsSavedList.Count)
                return false;
            for (int i = 0; i < initialSkills.Count; i++) {
                if (initialSkills[i] == null)
                {
                    return false;
                }
                if (initialSkills[i].SkillName != skillsSavedList[i].SkillName)
                    return false;
                if (!skillsSavedList[i].CheckEffectsInitiated() && !skillsSavedList[i].IsEffectsOverriden()) {
                    return false;
                }
            }
            return true;
        }

    }
    
}