﻿using System.Collections.Generic;
using CharacterSystem.Leveling;
using CharacterSystem.Skills;
using UnityEngine;

namespace CharacterSystem.CharacterBasics {
    public interface ICharacterData {
        string CharacterName { get; }
        Sprite CharacterIcon { get; }
        List<ICharacterStat> OriginalStats { get; }
        List<ICharacterStat> CurrentStats { get; }
        ILevelData LevelInformation { get; }
        List<ICharacterSkillData> Skills { get; }
    }
}