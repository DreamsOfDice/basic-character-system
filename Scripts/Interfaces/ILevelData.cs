namespace CharacterSystem.Leveling {
    public interface ILevelData {
        IExperience Experience { get; }
        object LevelGrowth { get; }
        int CurrentLevel { get; }
        int MaxLevel { get; }
    }
}