using System;

namespace CharacterSystem.Leveling {
    public interface IExperience {
        int CurrentExperience { get; }
        int TotalExperience { get; }
        int TargetExperience { get; }
        void GainExperience(int amount);
        void LoseExperience(int amount);
        void RegisterToExperienceChange(Action<IExperience> method);
    }
}