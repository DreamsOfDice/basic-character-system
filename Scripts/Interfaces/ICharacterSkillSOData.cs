using System.Collections.Generic;

namespace CharacterSystem.Skills {
    public interface ICharacterSkillSOData {
        string SkillName { get; }
        string SkillDescription { get; }
        List<ISkillCost> SkillCosts { get; }
        List<ICharacterSkillEffect> SkillEffects { get; }
    }
}