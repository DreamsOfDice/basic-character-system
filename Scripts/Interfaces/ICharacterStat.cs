﻿using System;
using UnityEngine;

namespace CharacterSystem.Skills {
    public interface ICharacterStat {
        object StatType { get; }
        int StatValue { get; set; }
        Sprite StatIcon { get; }
        void RegisterToStatChange(Action<ICharacterStat> method);
        ICharacterStat GetStatCopy();
    }
}