using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterSystem.Skills {

    public interface ICharacterSkillType {
        string SkillTypeName { get; }
        string SkillTypeDescription { get; }
        Sprite SkillTypeIcon { get; }
    }
}