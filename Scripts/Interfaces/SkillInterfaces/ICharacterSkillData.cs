﻿using System.Collections.Generic;
using UnityEngine;

namespace CharacterSystem.Skills {
    public interface ICharacterSkillData {
        string SkillName { get; }
        string SkillDescription { get; }
        Sprite SkillIcon { get; }
        List<ICharacterSkillType> SkillTypes { get; }
        List<ICharacterSkillEffect> Effects { get; }
    }
}