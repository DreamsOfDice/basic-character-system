using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterSystem.Skills
{
    public interface ISkillCost
    {
        string CostID { get; }
        int CostAmount { get; }
    }
}