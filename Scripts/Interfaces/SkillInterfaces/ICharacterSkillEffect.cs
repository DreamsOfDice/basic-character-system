﻿namespace CharacterSystem.Skills {
    public interface ICharacterSkillEffect {
        string EffectName { get; }
        string EffectDescription { get; }
        object TypeOfEffect { get; }
        int EffectValue { get; }
    }
}