using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills;
using UnityEditor;
using UnityEngine;

namespace CharacterSystem.Editor {
    [CustomPropertyDrawer(typeof(CharacterSkillInspectorContainer))]
    public class CharacterSkillContainerInspectorDrawer : PropertyDrawer {
        private int _choiceIndex;
        private string[] _choices;
        private readonly Dictionary<string, CharacterSkillSO> allSkillTemplates = new Dictionary<string, CharacterSkillSO>();
        private readonly CharacterCustomEditorBuilder<CharacterSkillSO> editorBuilderHelper = new CharacterCustomEditorBuilder<CharacterSkillSO>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            var statNameProperty = property.FindPropertyRelative("inspectorSkillName");
            var statTemplateProperty = property.FindPropertyRelative("template");
            var overideEffectsProperty = property.FindPropertyRelative("overrideSkillData");
            statTemplateProperty.objectReferenceValue =
                allSkillTemplates[_choices[property.FindPropertyRelative("index").intValue]];
            
            var overrideCheckboxRect = new Rect(position.x, position.y +EditorGUIUtility.singleLineHeight , 100, GetPropertyHeight(overideEffectsProperty,label)-EditorGUIUtility.singleLineHeight);

            
            overideEffectsProperty.boolValue = EditorGUI.Toggle(overrideCheckboxRect, overideEffectsProperty.boolValue);
            
            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);
           
            if (EditorGUI.EndChangeCheck()) {
                statNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                statTemplateProperty.objectReferenceValue =
                    allSkillTemplates[_choices[property.FindPropertyRelative("index").intValue]];
            }
        }

        private void SetupDropDown() {
            if (allSkillTemplates.Count == 0) {
                allSkillTemplates.Add("None", null);
            }

            foreach (var skill in editorBuilderHelper.GetAllInstances()) {
                if (allSkillTemplates.ContainsKey(skill.SkillName)) continue;
                allSkillTemplates.Add(skill.SkillName, skill);
            }

            _choices = allSkillTemplates.Keys.ToArray();
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            //int lineCount = 3;
            //return EditorGUIUtility.singleLineHeight * lineCount + EditorGUIUtility.standardVerticalSpacing * (lineCount-1);
            return EditorGUIUtility.singleLineHeight * 2;
        }
        
    }
}