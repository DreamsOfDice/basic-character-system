using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills;
using CharacterSystem.Templates;
using UnityEditor;
using UnityEngine;

namespace CharacterSystem.Editor
{
    [CustomPropertyDrawer(typeof(SkillCostNameContainer))]
    public class CharacterSkillCostDrawer : PropertyDrawer
    {
        private int _choiceIndex;
        private string[] _choices;

        private readonly Dictionary<string, SkillCostSO> allSkillCostsTemplates =
            new Dictionary<string, SkillCostSO>();
        private readonly CharacterCustomEditorBuilder<SkillCostSO> editorBuilderHelper = new CharacterCustomEditorBuilder<SkillCostSO>();

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            EditorGUI.BeginProperty(position, label, property);

            var skillCostInspectorNameProperty = property.FindPropertyRelative("skillCostInspectorName");
            var skillCostDisplayNameProperty = property.FindPropertyRelative("skillCostName");
            var skillCostDisplayDescriptionProperty = property.FindPropertyRelative("skillCostDescription");
            var skillCostTemplateProperty = property.FindPropertyRelative("skillCostBaseData");
            //var skillCostAmountProperty = property.FindPropertyRelative("skillCostAmount");

            var descriptionRect = new Rect(position.x, position.y +EditorGUIUtility.singleLineHeight , 300, GetPropertyHeight(skillCostDisplayDescriptionProperty,label)-(EditorGUIUtility.singleLineHeight));
            
           // var intFieldRect = new Rect(descriptionRect.width+position.x, descriptionRect.y+EditorGUIUtility.singleLineHeight  , 45,GetPropertyHeight(skillCostAmountProperty, label) -(3*EditorGUIUtility.singleLineHeight));
            
            
            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            var helpBoxStyle = new GUIStyle(EditorStyles.helpBox);
            var newNoteText = EditorGUI.TextField(descriptionRect,
                skillCostDisplayDescriptionProperty.stringValue,
                style: helpBoxStyle);
            //skillCostAmountProperty.intValue = EditorGUI.IntField(intFieldRect,skillCostAmountProperty.intValue );

            for (int i = 0; i < _choices.Length; i++) {
                if (_choices[i] == skillCostDisplayNameProperty.stringValue) {
                    property.FindPropertyRelative("index").intValue = i;
                }
            }
          

            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);
            
            if (EditorGUI.EndChangeCheck()) {
                skillCostInspectorNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                skillCostTemplateProperty.objectReferenceValue = allSkillCostsTemplates[_choices[property.FindPropertyRelative("index").intValue]];
                skillCostDisplayDescriptionProperty.stringValue =
                    allSkillCostsTemplates[_choices[property.FindPropertyRelative("index").intValue]].SkillCostDescription;
            }
            EditorGUI.EndProperty();
            
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            //int lineCount = 3;
            //return EditorGUIUtility.singleLineHeight * lineCount + EditorGUIUtility.standardVerticalSpacing * (lineCount-1);
            return EditorGUIUtility.singleLineHeight * 4;
        }
        
        private void SetupDropDown() {
            foreach (var skillCostInstances in editorBuilderHelper.GetAllInstances()) {
                if (allSkillCostsTemplates.ContainsKey(skillCostInstances.SkillCostName)) continue;
                allSkillCostsTemplates.Add(skillCostInstances.SkillCostName, skillCostInstances);
            }

            _choices = allSkillCostsTemplates.Keys.ToArray();
        }

    }
    
}