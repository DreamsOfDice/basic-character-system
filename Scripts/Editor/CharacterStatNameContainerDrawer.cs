using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Templates;
using UnityEditor;
using UnityEngine;

namespace CharacterSystem.Editor {
    [CustomPropertyDrawer(typeof(CharacterStatNameContainer))]
    public class CharacterStatNameContainerDrawer : PropertyDrawer {
       private int _choiceIndex;
        private string[] _choices;
        private readonly Dictionary<string, CharacterStatTypeSO> allStatTemplates = new Dictionary<string, CharacterStatTypeSO>();
        private readonly CharacterCustomEditorBuilder<CharacterStatTypeSO> editorBuilderHelper = new CharacterCustomEditorBuilder<CharacterStatTypeSO>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            var statNameProperty = property.FindPropertyRelative("inspectorStatName");
            var statTemplateProperty = property.FindPropertyRelative("template");
            if (statTemplateProperty.objectReferenceValue == null)
            {
                statTemplateProperty.objectReferenceValue = allStatTemplates[_choices[0]];
            }

            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);
            if (EditorGUI.EndChangeCheck()) {
                statNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                statTemplateProperty.objectReferenceValue =
                    allStatTemplates[_choices[property.FindPropertyRelative("index").intValue]];
            }
        }

        private void SetupDropDown() {
            foreach (var statType in editorBuilderHelper.GetAllInstances()) {
                if (allStatTemplates.ContainsKey(statType.statTypeName)) continue;
                allStatTemplates.Add(statType.statTypeName, statType);
            }

            _choices = allStatTemplates.Keys.ToArray();
        }
    } 
}