using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills;
using CharacterSystem.Templates;
using UnityEditor;
using UnityEngine;

namespace CharacterSystem.Editor {
    [CustomPropertyDrawer(typeof(CharacterSkillEffectTypeNameContainer))]
    public class CharacterEffectTypeContainerDrawer : PropertyDrawer {
        private int _choiceIndex;
        private string[] _choices;
        private readonly Dictionary<string, CharacterSkillEffectTypeSO> allStatTemplates = new Dictionary<string, CharacterSkillEffectTypeSO>();
        private readonly CharacterCustomEditorBuilder<CharacterSkillEffectTypeSO> editorBuilderHelper = new CharacterCustomEditorBuilder<CharacterSkillEffectTypeSO>();
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            var effectTypeNameProperty = property.FindPropertyRelative("inspectorEffectTypeName");
            var effectTypeTemplateProperty = property.FindPropertyRelative("template");

            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);
            if (EditorGUI.EndChangeCheck()) {
                effectTypeNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                effectTypeTemplateProperty.objectReferenceValue =
                    allStatTemplates[_choices[property.FindPropertyRelative("index").intValue]];
            }
            
        }
        private void SetupDropDown() {
            foreach (var effectType in editorBuilderHelper.GetAllInstances()) {
                if (allStatTemplates.ContainsKey(effectType.effectTypeName)) continue;
                allStatTemplates.Add(effectType.effectTypeName, effectType);
            }

            _choices = allStatTemplates.Keys.ToArray();
        }
        
    }
}