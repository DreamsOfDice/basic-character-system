using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills;
using UnityEditor;
using UnityEngine;

namespace CharacterSystem.Editor {
    [CustomPropertyDrawer(typeof(CharacterEffectNameContainer))]
    public class CharacterEffectNameContainerDrawer : PropertyDrawer{
        private int _choiceIndex;
        private string[] _choices;

        private readonly Dictionary<string, CharacterSkillEffectBaseInfoSO> allStatTemplates =
            new Dictionary<string, CharacterSkillEffectBaseInfoSO>();
        private readonly CharacterCustomEditorBuilder<CharacterSkillEffectBaseInfoSO> editorBuilderHelper = new CharacterCustomEditorBuilder<CharacterSkillEffectBaseInfoSO>();
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            EditorGUI.BeginProperty(position, label, property);

            var effectTypeNameProperty = property.FindPropertyRelative("effectInspectorName");
            var  effectDisplayNameProperty = property.FindPropertyRelative("effectName");
            var effectDisplayDescriptionProperty = property.FindPropertyRelative("effectDescription");
            var effectTypeTemplateProperty = property.FindPropertyRelative("skillEffectBaseInfoBaseData");

            var descriptionRect = new Rect(position.x, position.y +EditorGUIUtility.singleLineHeight , 300, GetPropertyHeight(effectDisplayDescriptionProperty,label)-EditorGUIUtility.singleLineHeight);

            
            
            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            var helpBoxStyle = new GUIStyle(EditorStyles.helpBox);
            var newNoteText = EditorGUI.TextField(descriptionRect,
                effectDisplayDescriptionProperty.stringValue,
                style: helpBoxStyle);

            for (int i = 0; i < _choices.Length; i++) {
                if (_choices[i] == effectDisplayNameProperty.stringValue) {
                    property.FindPropertyRelative("index").intValue = i;
                }
            }
          

            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);
            if (EditorGUI.EndChangeCheck()) {
                effectTypeNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                effectDisplayNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                effectTypeTemplateProperty.objectReferenceValue = allStatTemplates[_choices[property.FindPropertyRelative("index").intValue]];
                effectDisplayDescriptionProperty.stringValue =
                    allStatTemplates[_choices[property.FindPropertyRelative("index").intValue]].EffectDescription;
            }
            EditorGUI.EndProperty();
            
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            //int lineCount = 3;
            //return EditorGUIUtility.singleLineHeight * lineCount + EditorGUIUtility.standardVerticalSpacing * (lineCount-1);
            return EditorGUIUtility.singleLineHeight * 4;
        }
        
        private void SetupDropDown() {
            foreach (var effectName in editorBuilderHelper.GetAllInstances()) {
                if (allStatTemplates.ContainsKey(effectName.EffectName)) continue;
                allStatTemplates.Add(effectName.EffectName, effectName);
            }

            _choices = allStatTemplates.Keys.ToArray();
        }

        
    }
}