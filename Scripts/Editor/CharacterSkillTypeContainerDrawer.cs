using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CharacterSystem.Skills;
using CharacterSystem.Templates;
using UnityEditor;
using UnityEngine;

namespace CharacterSystem.Editor
{
    [CustomPropertyDrawer(typeof(CharacterSkillTypeContainer))]
    public class CharacterSkillTypeContainerDrawer : PropertyDrawer
    {
        private int _choiceIndex;
        private string[] _choices;
        private readonly Dictionary<string, CharacterSkillTypeSO> allStatTemplates = new Dictionary<string, CharacterSkillTypeSO>();
        private readonly CharacterCustomEditorBuilder<CharacterSkillTypeSO> editorBuilderHelper = new CharacterCustomEditorBuilder<CharacterSkillTypeSO>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            var statNameProperty = property.FindPropertyRelative("inspectorSkillTypeName");
            var statTemplateProperty = property.FindPropertyRelative("scriptableObjTemplate");

            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);
            if (EditorGUI.EndChangeCheck()) {
                statNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                statTemplateProperty.objectReferenceValue =
                    allStatTemplates[_choices[property.FindPropertyRelative("index").intValue]];
            }
        }

        private void SetupDropDown() {
            foreach (var skillType in editorBuilderHelper.GetAllInstances()) {
                if (allStatTemplates.ContainsKey(skillType.skillTypeName)) continue;
                allStatTemplates.Add(skillType.skillTypeName, skillType);
            }

            _choices = allStatTemplates.Keys.ToArray();
        }
    }
}